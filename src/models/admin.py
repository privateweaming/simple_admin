# coding: utf8

from datetime import datetime as dt

from sqlalchemy import Column, ForeignKey, Table, desc
from sqlalchemy.dialects.sqlite import \
            BLOB, BOOLEAN, CHAR, DATE, DATETIME, DECIMAL, FLOAT, \
            INTEGER, NUMERIC, SMALLINT, TEXT, TIME, TIMESTAMP, \
            VARCHAR
from sqlalchemy.orm import backref, relationship

from . import DeclarativeBase, metadata


class ModelCommonMixin(object):
    id = Column(INTEGER, primary_key=True)
    created_time = Column(DATETIME, default=dt.now)
    modified_time = Column(DATETIME, default=dt.now, onupdate=dt.now().strftime('%Y-%m-%d %X'))
    active = Column(BOOLEAN, default=True)


class User(ModelCommonMixin, DeclarativeBase):
    __tablename__ = 'user'

    name = Column(VARCHAR)
    show_name = Column(VARCHAR)
    org = Column(VARCHAR)

    def __str__(self):
        return '%s(%s)' % (self.show_name, self.name)


class Project(ModelCommonMixin, DeclarativeBase):
    __tablename__ = 'project'

    name = Column(VARCHAR)
    url = Column(VARCHAR)

    def __str__(self):
        return self.name


class WeekLog(ModelCommonMixin, DeclarativeBase):
    __tablename__ = 'week_log'

    user_id = Column(INTEGER, ForeignKey('user.id'), nullable=False)
    project_id = Column(INTEGER, ForeignKey('project.id'), nullable=False)
    start = Column(DATE, nullable=False)
    end = Column(DATE, nullable=False)
    description = Column(VARCHAR, nullable=False)

    user = relationship(User)
    project = relationship(Project, backref="logs")

    def __str__(self):
        return '%s %s: %s' % (self.id, self.user.show_name, self.description)

metadata.create_all()
