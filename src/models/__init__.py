# coding: utf8

from sqlalchemy import create_engine, MetaData
from sqlalchemy.pool import NullPool
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:///task.db', echo=False, poolclass=NullPool)
maker = sessionmaker(autocommit=False, autoflush=True, bind=engine)

DBSession = scoped_session(maker)
DeclarativeBase = declarative_base(bind=engine)
metadata = DeclarativeBase.metadata
