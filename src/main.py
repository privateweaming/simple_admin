# coding: utf8

from flask import Flask, redirect
from flask_admin import Admin, BaseView, expose
from flask_admin.contrib.sqla import ModelView

from models import DBSession
from models.admin import User, Project, WeekLog

app = Flask(__name__)


@app.route('/')
def hello():
    return redirect("/admin", code=302)


class UserView(ModelView):
    column_exclude_list = ['user_id', 'project_id', 'created_id', 'modified_id', 'created_time']

    def __init__(self, Model, session, **kwargs):
        # You can pass name and other parameters if you want to
        super(UserView, self).__init__(Model, session, **kwargs)


admin = Admin(app, name='Task list', template_mode='bootstrap3')
admin.add_view(UserView(User, DBSession))
admin.add_view(UserView(Project, DBSession))
admin.add_view(UserView(WeekLog, DBSession))


class VisualizeView(BaseView):
    @expose('/')
    def index(self):
        return self.render('visualize.html', logs=DBSession.query(WeekLog).all())


admin.add_view(VisualizeView(name='Visualize', endpoint='visualize'))

if __name__ == "__main__":
    app.secret_key = 'hello world'
    app.config['SESSION_TYPE'] = 'filesystem'

    # DBSession.init_app(app)

    app.debug = True
    app.run(host='0.0.0.0')
