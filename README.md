## simple admin

Demonstration of flask-admin

## Usage

1. `git clone https://github.com/weaming/simple_admin`
2. Create virtual environment: `virtualenv env`
3. Activate the virtualenv(Different based on your system)
4. Install dependencies: `pip install -r requirements.txt`
5. Start the demo: `python ./src/main.py`(will create sqlite3 file `task.db` in `./src`)

## Links

- [Official document of flask-admin](https://flask-admin.readthedocs.io/en/latest/)
- [Hacking the model view](https://flask-admin.readthedocs.io/en/latest/api/mod_model/#flask_admin.model.BaseModelView)
